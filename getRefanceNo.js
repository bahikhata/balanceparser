const regx1 = /Refno (\d+)/;
const regx2 = /Reference No\.:([A-Z0-9]+)/;
const regx3 = /IMPS Ref no (\d+)/;
const regx4 = /SBI Txn Ref No (\w+)/;
const regx5 = /UTR (\w+)/;
const regx6 = /Ref no (\d+)/;

const getRefanceNo = (message, bankName) => {

    if (bankName === 'SBI') {
        let match1 = message.match(regx1);
        let match2 = message.match(regx2);
        let match3 = message.match(regx3);
        let match4 = message.match(regx4);
        let match5 = message.match(regx5);
        let match6 = message.match(regx6);

        if (match1) {
            return match1[1];
        }

        if (match2) {
            return match2[1];
        }
        if (match3) {
            return match3[1];
        }
        if (match4) {
            return match4[1];
        }
        if (match5) {
            return match5[1].slice(-10);
        }
        if (match6) {
            return match6[1];
        }

        return "......";
    }
    return "......";
}

module.exports = getRefanceNo;