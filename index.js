const extractData = require("./extractData");
const getBankName = require("./getBankName");
const accounting = require("accounting");
const getAccNoAndBal = require("./getAccNoAndBal");

const estimatedBal = (data, accno) => {
    //Get Balance 
    let availabelBalAndDate = data.filter(item => item?.body?.includes(accno) && getAccNoAndBal(item?.body, getBankName(item?.address)) !== null).map(item => { return { bal: parseFloat(getAccNoAndBal(item?.body, getBankName(item?.address)).toString().replace(/,/g, "")), date: item?.date } });
    console.log(availabelBalAndDate);

    availabelBalAndDate = availabelBalAndDate.filter(item => item?.bal !== null && !isNaN(item?.bal) && item?.bal !== 0);
    console.log(availabelBalAndDate);

    data = data.filter(item => item?.body?.includes(accno));
    console.log(data);

    let EstimatedBal=0;
    let afterAmount=0;
    let beforeAmount=0;
    
    try {
        if (availabelBalAndDate.length > 0 && data.length > 0) {
            let date = availabelBalAndDate[0]?.date;
    
            data.forEach(item => {
                const itemDate = item?.date;
                let bankName = item?.address?.split('-')[1];
                bankName = getBankName(bankName);
                let info = extractData(item?.body,bankName);
                let amount = parseFloat(info?.amount?.toString().replace(/,/g, ""));
                if (isNaN(amount)) {
                    amount = 0;
                }
                let type = info?.type?.toLowerCase();

                if (itemDate > date ) {
                    if (type === 'credited') {
                        afterAmount += amount;
                    } else {
                        afterAmount -= amount;
                    }
                }
                else if (itemDate < date) {

                    if (type === 'credited') {
                        beforeAmount -= amount;
                    } else {
                        beforeAmount += amount;
                    }
                }
            });
    
        }
    } catch (error) {
        console.log(error);
    }

    EstimatedBal = availabelBalAndDate[0]?.bal + afterAmount + beforeAmount;
    
    if(EstimatedBal<0){
        EstimatedBal=0;
    }

    EstimatedBal = accounting.formatMoney(EstimatedBal, {
        symbol: "",
        format: "%v",
        precision: 0,
    });

    return EstimatedBal;
}

var data = [
    {"address": "AD-CBSSBI", "body": "Your A/C XXXXX361282 Debited INR 14,000.00 on 10/05/24 -Transferred to Mr. DIVYANSHUGOEL. Avl Balance INR 6,213.13-SBI", "date": 1715279401000, "id": 1}, 
    {"address": "AD-SBIUPI", "body": "Dear UPI user A/C X1282 debited by 383.5 on date 10May24 trf to Grey Swift Priva Refno 413193110180. If not u? call 1800111109. -SBI", "date": 1715280602000, "id": 2},
    {"address": "BX-CBSSBI", "body": "Your A/C XXXXX361282 Credited INR 1,00,000.00 on 10/05/24 -Deposit by transfer from NINJA MONK LABS. Avl Bal INR 1,05,829.63-SBI", "date": 1715281203000, "id": 3}, 
    {"address": "VM-SBIINB", "body": "Dear Customer, Your a/c no. XXXXXXXX1282 is credited by Rs.250000.00 on 10-05-24 by a/c linked to mobile 9XXXXXX567-NITISH VERMA (IMPS Ref no 413121331133).If not done by you, call 1800111109. -SBI", "date": 1715281804000, "id": 4},
    {"address": "AX-SBIUPI", "body": "Dear UPI user A/C X1282 debited by 500.0 on date 11May24 trf to Innofin Solution Refno 413238380040. If not u? call 1800111109. -SBI", "date": 1715365800000, "id": 6},
    {"address": "AX-SBIUPI", "body": "Dear UPI user A/C X1282 debited by 3933.0 on date 12May24 trf to DUSA SURESH DEVA Refno 413301440541. If not u? call 1800111109. -SBI", "date": 1715452200000, "id": 7},
    {"address": "VM-SBIINB", "body": "Dear Customer, Your a/c no. XXXXXXXX1282 is credited by Rs.300.00 on 13-05-24 by a/c linked to mobile 0XXXXXX000-GOOGLEINDIADIGITAL (IMPS Ref no 413421718953).If not done by you, call 1800111109. -SBI", "date": 1715538600000, "id": 8},
    {"address": "AX-SBIUPI", "body": "Dear UPI user A/C X1282 debited by 1000.0 on date 14May24 trf to SATYAM Refno 413555020242. If not u? call 1800111109. -SBI", "date": 1715625001000, "id": 9},
    {"address": "VM-SBIUPI", "body": "Dear UPI user A/C X1282 debited by 1180.0 on date 14May24 trf to Precisa Refno 413568761350. If not u? call 1800111109. -SBI", "date": 1715625002000, "id": 10}, 
    {"address": "JK-SBIUPI", "body": "Dear SBI UPI User, ur A/cX1282 credited by Rs18500 on 14May24 by (Ref no 413523056185)", "date": 1715625003000, "id": 11}
];

var pastBalAndDate = [{"accNo":"1282","bal":6213.13,"date":"2024-05-10T00:00:00","bankName":"SBI"}];
var accno = "1282"; //getAccountNo(data[0]);

console.log(estimatedBal(data, accno));


/*
const uniqueAccNoAndBal = (accAndBal) => {
    const uniqAcc = new Set();
    const AccNoAndBal = [];
    const uniqueAccounts = {};

    accAndBal.forEach(item => {
      if (item?.accNo && item.bankName !== "Other" && parseFloat(item.bal.replace ? item.bal.replace(',', '') : item.bal) >= 0) {
        if (!uniqAcc.has(item.accNo)) {
          uniqAcc.add(item.accNo);
          AccNoAndBal.push({
            accNo: item.accNo,
            bal: parseFloat(item.bal.replace ? item.bal.replace(',', '') : item.bal),
            date: item.date,
            bankName: item.bankName
          });
        }
        uniqueAccounts[item.accNo] = item.bankName;
      }
    });

    const AccNoBankName = Object.keys(uniqueAccounts).map(accNo => ({
      accNo,
      bankName: uniqueAccounts[accNo]
    }));

    return { AccNoAndBal, AccNoBankName };
  }
*/
const getAccountNo = (sms) => {
    var message = sms.body;
    let regx1 = /(?:A\/c|A\/C)\s+\w*(\d{4})/gi;
    let regx2 = /A\/c([A-Z0-9]+)/
    let regx3 = /a\/c\s+no\.\s+([A-Z0-9]+)/i;
    let regx4 = /A\/c\s+No\s+([A-Z0-9]+)/
    let regx5 = /a\/c\s+no\.\s+ending\s+with\s+\*{3}(\d{4})/i;
    let regx6 = /A\/c\s\.{3}(\d+)/;

    let accNo = message.match(regx1);
    let accNo2 = message.match(regx2);
    let accNo3 = message.match(regx3);
    let accNo4 = message.match(regx4);
    let accNo5 = message.match(regx5);
    let accNo6 = regx6.exec(message);
    if (accNo5) {
        accNo5 = accNo5[1];
    }
    if (accNo6) {
        accNo6 = accNo6[1];
    }

    return { accNo: accNo6 || accNo5 || accNo || accNo2 || accNo3 || accNo4 }
}

var rawData = [
    {"_id":298,"thread_id":154,"address":"AX-khatab","date":1715709052634,"date_sent":1715709050000,"protocol":0,"read":0,"status":-1,"type":1,"reply_path_present":0,"body":"OTP: 5139 is your Bahikhata account verification code.","service_center":"+919892958696","locked":0,"sub_id":1,"phone_id":1,"error_code":0,"creator":"com.android.mms","seen":0,"priority":-1,"oppo_drafts":1,"oppo_mass":0,"oppo_timer":0,"oppo_groupaddress":"AX-khatab","oppo_collected":0,"oppo_sub_date":1715709050000,"oppo_service_message_sms_type":0,"bubble":"{\"msgId\":\"-1\",\"date\":1715709052743,\"progress\":false,\"title\":\"Verification Reminder\",\"source\":0,\"entities\":[{\"id\":\"-1\",\"from\":0,\"index\":5,\"matchedWords\":\"5139\",\"showType\":0,\"actions\":[{\"action\":16,\"buttonText\":\"Copy code\",\"clip\":\"5139\",\"icon\":\"http:\\\/\\\/img.teddymobile.cn\\\/2015\\\/02\\\/02\\\/0778d5f6b39602762f6af1bbcf865512_60X60.png\",\"position\":-1,\"type\":0,\"service\":-1,\"tagType\":0,\"verification_code\":\"5139\",\"verification_show\":\"Copy code\",\"verification_name\":\"Code\",\"verification_country\":1}]}],\"cardBase\":\"{\\\"type\\\":33,\\\"title\\\":{\\\"Verification Reminder\\\":\\\"\\\"},\\\"subtitle\\\":{\\\"OTP\\\":\\\"5139\\\"},\\\"data\\\":{\\\"Message\\\":\\\"OTP: 5139 is your Bahikhata account verification code.\\\"},\\\"time_stamp\\\":-1,\\\"matched_id\\\":-1,\\\"n_type\\\":\\\"13FF75FF\\\",\\\"from\\\":\\\"converted\\\",\\\"choose\\\":0,\\\"actions\\\":\\\"[{\\\\\\\"action\\\\\\\":16,\\\\\\\"buttonText\\\\\\\":\\\\\\\"Copy code\\\\\\\",\\\\\\\"clip\\\\\\\":\\\\\\\"5139\\\\\\\",\\\\\\\"icon\\\\\\\":\\\\\\\"http:\\\\\\\\\\\\\\\/\\\\\\\\\\\\\\\/img.teddymobile.cn\\\\\\\\\\\\\\\/2015\\\\\\\\\\\\\\\/02\\\\\\\\\\\\\\\/02\\\\\\\\\\\\\\\/0778d5f6b39602762f6af1bbcf865512_60X60.png\\\\\\\",\\\\\\\"position\\\\\\\":-1,\\\\\\\"type\\\\\\\":0,\\\\\\\"service\\\\\\\":-1,\\\\\\\"tagType\\\\\\\":0,\\\\\\\"verification_code\\\\\\\":\\\\\\\"5139\\\\\\\",\\\\\\\"verification_show\\\\\\\":\\\\\\\"Copy code\\\\\\\",\\\\\\\"verification_name\\\\\\\":\\\\\\\"Code\\\\\\\",\\\\\\\"verification_country\\\\\\\":1}]\\\"}\"}","deleted":0,"sync_state":0,"oppo_sms_type":1,"block_type":0,"favourite":0,"rcs_msg_type":-1,"rcs_chat_type":-1,"rcs_burn":-1,"rcs_is_download":0,"rcs_file_size":0,"rcs_media_played":0,"rcs_audio_read":0},
    {"_id":297,"thread_id":158,"address":"VM-MCXINS","date":1715704474177,"date_sent":1715704472000,"protocol":0,"read":0,"status":-1,"type":1,"reply_path_present":0,"body":"Client Funds on 04\/05\/2024.MCX 0. Net Exchanges 1.Clear Exchanges 1.MCX M-ID 12685-Angel One Limited.UCC V52725491. -MCXLTD","service_center":"+919811002486","locked":0,"sub_id":1,"phone_id":1,"error_code":0,"creator":"com.android.mms","seen":0,"priority":-1,"oppo_drafts":1,"oppo_mass":0,"oppo_timer":0,"oppo_groupaddress":"VM-MCXINS","oppo_collected":0,"oppo_sub_date":1715704472000,"oppo_service_message_sms_type":0,"bubble":"{\"url_type\":2,\"bubble\":\"{\\\"msgId\\\":\\\"-1\\\",\\\"date\\\":1715704474274,\\\"progress\\\":false,\\\"source\\\":0,\\\"entities\\\":[{\\\"id\\\":\\\"-4\\\",\\\"from\\\":0,\\\"index\\\":16,\\\"matchedWords\\\":\\\"04\\\\\\\/05\\\\\\\/2024\\\",\\\"showType\\\":2,\\\"actions\\\":[{\\\"action\\\":21,\\\"buttonText\\\":\\\"时间提醒\\\",\\\"position\\\":-1,\\\"type\\\":0,\\\"service\\\":0,\\\"tagType\\\":0,\\\"body\\\":\\\"Client Funds on 04\\\\\\\/05\\\\\\\/2024.MCX 0. Net Exchanges 1.Clear Exchanges 1.MCX M-ID 12685-Angel One Limited.UCC V52725491. -MCXLTD\\\",\\\"time_key\\\":1714761000000,\\\"all_day\\\":true,\\\"end_time_key\\\":1714761000000}]}]}\"}","deleted":0,"sync_state":0,"oppo_sms_type":0,"block_type":0,"favourite":0,"rcs_msg_type":-1,"rcs_chat_type":-1,"rcs_burn":-1,"rcs_is_download":0,"rcs_file_size":0,"rcs_media_played":0,"rcs_audio_read":0},
    {"_id":296,"thread_id":157,"address":"JM-NSESMS","date":1715702121198,"date_sent":1715702118000,"protocol":0,"read":0,"status":-1,"type":1,"reply_path_present":0,"body":"ANGEL ONE LIMITED on 20-04-24 reported your Fund bal Rs.0.82 & Securities bal 0. This excludes your Bank, DP & PMS bal with the broker-NSE","service_center":"+917021075042","locked":0,"sub_id":1,"phone_id":1,"error_code":0,"creator":"com.android.mms","seen":0,"priority":-1,"oppo_drafts":1,"oppo_mass":0,"oppo_timer":0,"oppo_groupaddress":"JM-NSESMS","oppo_collected":0,"oppo_sub_date":1715702118000,"oppo_service_message_sms_type":0,"bubble":"{\"msgId\":\"-1\",\"date\":1715702121282,\"progress\":false,\"source\":0,\"entities\":[{\"id\":\"-4\",\"from\":0,\"index\":21,\"matchedWords\":\"20-04-24\",\"showType\":2,\"actions\":[{\"action\":21,\"buttonText\":\"时间提醒\",\"position\":-1,\"type\":0,\"service\":0,\"tagType\":0,\"body\":\"ANGEL ONE LIMITED on 20-04-24 reported your Fund bal Rs.0.82 & Securities bal 0. This excludes your Bank, DP & PMS bal with the broker-NSE\",\"time_key\":1713551400000,\"all_day\":true,\"end_time_key\":1713551400000}]}]}","deleted":0,"sync_state":0,"oppo_sms_type":0,"block_type":0,"favourite":0,"rcs_msg_type":-1,"rcs_chat_type":-1,"rcs_burn":-1,"rcs_is_download":0,"rcs_file_size":0,"rcs_media_played":0,"rcs_audio_read":0},
    {"_id":295,"thread_id":156,"address":"AZ-ARWYNK","date":1715685044557,"date_sent":1715685039000,"protocol":0,"read":0,"status":-1,"type":1,"reply_path_present":0,"body":"Bina rukawat music aur latest gaano ko miss kar rahein hain? Sirf Rs49 mein Wynk Premium ke saath anand lein unlimited ad-free music aur hellotune ka, saath hi karein gaane download. Abhi Upgrade karein. Click open.wynk.in\/dc","service_center":"+919831029344","locked":0,"sub_id":1,"phone_id":1,"error_code":0,"creator":"com.android.mms","seen":0,"priority":-1,"oppo_drafts":1,"oppo_mass":0,"oppo_timer":0,"oppo_groupaddress":"AZ-ARWYNK","oppo_collected":0,"oppo_sub_date":1715685039000,"oppo_service_message_sms_type":0,"bubble":"{\"url_type\":2,\"bubble\":\"{\\\"msgId\\\":\\\"-1\\\",\\\"date\\\":1715685044688,\\\"progress\\\":false,\\\"source\\\":0,\\\"entities\\\":[{\\\"id\\\":\\\"-7\\\",\\\"from\\\":0,\\\"index\\\":210,\\\"matchedWords\\\":\\\"open.wynk.in\\\\\\\/dc\\\",\\\"showType\\\":2,\\\"actions\\\":[{\\\"action\\\":3,\\\"buttonText\\\":\\\"打开链接\\\",\\\"url\\\":\\\"open.wynk.in\\\\\\\/dc\\\",\\\"position\\\":-1,\\\"type\\\":0,\\\"service\\\":-1,\\\"tagType\\\":0}]}]}\"}","deleted":0,"sync_state":0,"oppo_sms_type":0,"block_type":0,"favourite":0,"rcs_msg_type":-1,"rcs_chat_type":-1,"rcs_burn":-1,"rcs_is_download":0,"rcs_file_size":0,"rcs_media_played":0,"rcs_audio_read":0},
    {"_id":294,"thread_id":155,"address":"+919680923327","date":1715678363363,"date_sent":0,"read":1,"status":-1,"type":2,"body":"Can I call you back later?","locked":0,"sub_id":1,"phone_id":-1,"error_code":0,"creator":"com.android.server.telecom","seen":1,"priority":-1,"oppo_drafts":1,"oppo_mass":0,"oppo_timer":0,"oppo_collected":0,"oppo_service_message_sms_type":0,"deleted":0,"sync_state":0,"oppo_sms_type":0,"block_type":0,"favourite":0,"rcs_msg_type":-1,"rcs_chat_type":-1,"rcs_burn":-1,"rcs_is_download":0,"rcs_file_size":0,"rcs_media_played":0,"rcs_audio_read":0},
    {"_id":293,"thread_id":154,"address":"AX-khatab","date":1715675809232,"date_sent":1715675806000,"protocol":0,"read":0,"status":-1,"type":1,"reply_path_present":0,"body":"OTP: 4597 is your Bahikhata account verification code.","service_center":"+919892958696","locked":0,"sub_id":1,"phone_id":1,"error_code":0,"creator":"com.android.mms","seen":0,"priority":-1,"oppo_drafts":1,"oppo_mass":0,"oppo_timer":0,"oppo_groupaddress":"AX-khatab","oppo_collected":0,"oppo_sub_date":1715675806000,"oppo_service_message_sms_type":0,"bubble":"{\"msgId\":\"-1\",\"date\":1715675809326,\"progress\":false,\"title\":\"Verification Reminder\",\"source\":0,\"entities\":[{\"id\":\"-1\",\"from\":0,\"index\":5,\"matchedWords\":\"4597\",\"showType\":0,\"actions\":[{\"action\":16,\"buttonText\":\"Copy code\",\"clip\":\"4597\",\"icon\":\"http:\\\/\\\/img.teddymobile.cn\\\/2015\\\/02\\\/02\\\/0778d5f6b39602762f6af1bbcf865512_60X60.png\",\"position\":-1,\"type\":0,\"service\":-1,\"tagType\":0,\"verification_code\":\"4597\",\"verification_show\":\"Copy code\",\"verification_name\":\"Code\",\"verification_country\":1}]}],\"cardBase\":\"{\\\"type\\\":33,\\\"title\\\":{\\\"Verification Reminder\\\":\\\"\\\"},\\\"subtitle\\\":{\\\"OTP\\\":\\\"4597\\\"},\\\"data\\\":{\\\"Message\\\":\\\"OTP: 4597 is your Bahikhata account verification code.\\\"},\\\"time_stamp\\\":-1,\\\"matched_id\\\":-1,\\\"n_type\\\":\\\"13FF75FF\\\",\\\"from\\\":\\\"converted\\\",\\\"choose\\\":0,\\\"actions\\\":\\\"[{\\\\\\\"action\\\\\\\":16,\\\\\\\"buttonText\\\\\\\":\\\\\\\"Copy code\\\\\\\",\\\\\\\"clip\\\\\\\":\\\\\\\"4597\\\\\\\",\\\\\\\"icon\\\\\\\":\\\\\\\"http:\\\\\\\\\\\\\\\/\\\\\\\\\\\\\\\/img.teddymobile.cn\\\\\\\\\\\\\\\/2015\\\\\\\\\\\\\\\/02\\\\\\\\\\\\\\\/02\\\\\\\\\\\\\\\/0778d5f6b39602762f6af1bbcf865512_60X60.png\\\\\\\",\\\\\\\"position\\\\\\\":-1,\\\\\\\"type\\\\\\\":0,\\\\\\\"service\\\\\\\":-1,\\\\\\\"tagType\\\\\\\":0,\\\\\\\"verification_code\\\\\\\":\\\\\\\"4597\\\\\\\",\\\\\\\"verification_show\\\\\\\":\\\\\\\"Copy code\\\\\\\",\\\\\\\"verification_name\\\\\\\":\\\\\\\"Code\\\\\\\",\\\\\\\"verification_country\\\\\\\":1}]\\\"}\"}","deleted":0,"sync_state":0,"oppo_sms_type":1,"block_type":0,"favourite":0,"rcs_msg_type":-1,"rcs_chat_type":-1,"rcs_burn":-1,"rcs_is_download":0,"rcs_file_size":0,"rcs_media_played":0,"rcs_audio_read":0},
    {"_id":292,"thread_id":153,"address":"AX-QNTAMC","date":1715668474199,"date_sent":1715668472000,"protocol":0,"read":0,"status":-1,"type":1,"reply_path_present":0,"body":"Dear Investor, Your SIP installment in Folio-XXXXXXX8893 under quant Small Cap Fund - Direct Plan Growth for Rs. 999.95 is processed @NAV of Rs. 258.3163 and 3.871 units are allotted. Rgds,quant Mutual Fund.","service_center":"+919892958696","locked":0,"sub_id":1,"phone_id":1,"error_code":0,"creator":"com.android.mms","seen":0,"priority":-1,"oppo_drafts":1,"oppo_mass":0,"oppo_timer":0,"oppo_groupaddress":"AX-QNTAMC","oppo_collected":0,"oppo_sub_date":1715668472000,"oppo_service_message_sms_type":0,"bubble":"0","deleted":0,"sync_state":0,"oppo_sms_type":0,"block_type":0,"favourite":0,"rcs_msg_type":-1,"rcs_chat_type":-1,"rcs_burn":-1,"rcs_is_download":0,"rcs_file_size":0,"rcs_media_played":0,"rcs_audio_read":0},
    {"_id":291,"thread_id":152,"address":"AD-FEDBNK","date":1715593876066,"date_sent":1715593872000,"protocol":0,"read":0,"status":-1,"type":1,"reply_path_present":0,"body":"Dear VYANKATESH GANGADHAR TUPPALWAD, your accounts opened through Fi Money app, under the Customer ID *****1564 have become due for periodic KYC review. If there are no changes in your KYC details submitted to the Bank, please confirm by sending an SMS 'KYC Y' to 9008915353 from your registered mobile number latest by 14-06-2024 and avoid debit restrictions in your account. Thank you for your cooperation\n-Federal Bank.","service_center":"+919892958696","locked":0,"sub_id":1,"phone_id":1,"error_code":0,"creator":"com.android.mms","seen":0,"priority":-1,"oppo_drafts":1,"oppo_mass":0,"oppo_timer":0,"oppo_groupaddress":"AD-FEDBNK","oppo_collected":0,"oppo_sub_date":1715593872000,"oppo_service_message_sms_type":0,"bubble":"{\"msgId\":\"-1\",\"date\":1715593876163,\"progress\":false,\"source\":0,\"entities\":[{\"id\":\"-4\",\"from\":0,\"index\":320,\"matchedWords\":\"14-06-2024\",\"showType\":2,\"actions\":[{\"action\":21,\"buttonText\":\"时间提醒\",\"position\":-1,\"type\":0,\"service\":0,\"tagType\":0,\"body\":\"Dear VYANKATESH GANGADHAR TUPPALWAD, your accounts opened through Fi Money app, under the Customer ID *****1564 have become due for periodic KYC review. If there are no changes in your KYC details submitted to the Bank, please confirm by sending an SMS 'KYC Y' to 9008915353 from your registered mobile number latest by 14-06-2024 and avoid debit restrictions in your account. Thank you for your cooperation\\n-Federal Bank.\",\"time_key\":1718303400000,\"all_day\":true,\"end_time_key\":1718303400000}]},{\"id\":\"-6\",\"from\":0,\"index\":264,\"matchedWords\":\"9008915353\",\"showType\":2,\"actions\":[{\"action\":22,\"buttonText\":\"拨打电话\",\"number\":\"9008915353\",\"position\":-1,\"type\":0,\"service\":-1,\"tagType\":0}]}]}","deleted":0,"sync_state":0,"oppo_sms_type":0,"block_type":0,"favourite":0,"rcs_msg_type":-1,"rcs_chat_type":-1,"rcs_burn":-1,"rcs_is_download":0,"rcs_file_size":0,"rcs_media_played":0,"rcs_audio_read":0},
    {"_id":290,"thread_id":116,"address":"VM-BOBTXN","date":1715572261674,"date_sent":1715572257000,"protocol":0,"read":1,"status":-1,"type":1,"reply_path_present":0,"body":"रक्‍कम रु. 1 खाता सं.  5105 मध्‍ये UPI\/413426252052च्‍या  माध्यमातून angelmfcpupa_in द्वारा जमा केली गेली. कुल शेष रक्‍कम: रु. 906.27CR. उपलब्ध शेष रक्‍कम रु. 906.27(13-05-2024 09:15:22). -बॅंक ऑफ बडोदा","service_center":"+919811002486","locked":0,"sub_id":1,"phone_id":1,"error_code":0,"creator":"com.android.mms","seen":1,"priority":-1,"oppo_drafts":1,"oppo_mass":0,"oppo_timer":0,"oppo_groupaddress":"VM-BOBTXN","oppo_collected":0,"oppo_sub_date":1715572257000,"oppo_service_message_sms_type":0,"bubble":"{\"msgId\":\"-1\",\"date\":1715572261789,\"progress\":false,\"source\":0,\"entities\":[{\"id\":\"-4\",\"from\":0,\"index\":166,\"matchedWords\":\"13-05-2024 09:15:22\",\"showType\":2,\"actions\":[{\"action\":21,\"buttonText\":\"时间提醒\",\"position\":-1,\"type\":0,\"service\":0,\"tagType\":0,\"body\":\"रक्‍कम रु. 1 खाता सं.  5105 मध्‍ये UPI\\\/413426252052च्‍या  माध्यमातून angelmfcpupa_in द्वारा जमा केली गेली. कुल शेष रक्‍कम: रु. 906.27CR. उपलब्ध शेष रक्‍कम रु. 906.27(13-05-2024 09:15:22). -बॅंक ऑफ बडोदा\",\"time_key\":1715571922000,\"all_day\":false,\"end_time_key\":1715571922000}]}]}","deleted":0,"sync_state":0,"oppo_sms_type":0,"block_type":0,"favourite":0,"rcs_msg_type":-1,"rcs_chat_type":-1,"rcs_burn":-1,"rcs_is_download":0,"rcs_file_size":0,"rcs_media_played":0,"rcs_audio_read":0},
    {"_id":289,"thread_id":116,"address":"VM-BOBTXN","date":1715571939726,"date_sent":1715571936000,"protocol":0,"read":1,"status":-1,"type":1,"reply_path_present":0,"body":"रक्‍कम रु.1000 खाता सं.5105 मधून UPI\/413465353347 ला हस्तांतरित केली गेली.कुल शेष रक्‍कम रु..906.27CR. उपलब्ध शेष रक्‍कम रु. 906.27(13-05-2024 09:09:42).- बॅंक ऑफ बडोदा","service_center":"+919811002486","locked":0,"sub_id":1,"phone_id":1,"error_code":0,"creator":"com.android.mms","seen":1,"priority":-1,"oppo_drafts":1,"oppo_mass":0,"oppo_timer":0,"oppo_groupaddress":"VM-BOBTXN","oppo_collected":0,"oppo_sub_date":1715571936000,"oppo_service_message_sms_type":0,"bubble":"{\"msgId\":\"-1\",\"date\":1715571939808,\"progress\":false,\"source\":0,\"entities\":[{\"id\":\"-4\",\"from\":0,\"index\":132,\"matchedWords\":\"13-05-2024 09:09:42\",\"showType\":2,\"actions\":[{\"action\":21,\"buttonText\":\"时间提醒\",\"position\":-1,\"type\":0,\"service\":0,\"tagType\":0,\"body\":\"रक्‍कम रु.1000 खाता सं.5105 मधून UPI\\\/413465353347 ला हस्तांतरित केली गेली.कुल शेष रक्‍कम रु..906.27CR. उपलब्ध शेष रक्‍कम रु. 906.27(13-05-2024 09:09:42).- बॅंक ऑफ बडोदा\",\"time_key\":1715571582000,\"all_day\":false,\"end_time_key\":1715571582000}]}]}","deleted":0,"sync_state":0,"oppo_sms_type":0,"block_type":0,"favourite":0,"rcs_msg_type":-1,"rcs_chat_type":-1,"rcs_burn":-1,"rcs_is_download":0,"rcs_file_size":0,"rcs_media_played":0,"rcs_audio_read":0}
];

let regex = /[Aa]\/[Cc]|A[\/cC]|Acc/;

let filteredTxns = rawData.filter((item) => regex.test(item?.body)).map((item) => ({
    date: item?.date,
    body: item?.body,
    address: item?.address
}));

console.log(filteredTxns);

var data2 = [
    {"accNo": "1282", "bal": "6,213.13", "bankName": "SBI", "date": "2024-05-10T00:00:00"}, 
    {"accNo": "1282", "bal": 0, "bankName": "SBI", "date": "2024-05-10T00:00:00"}, 
    {"accNo": "1282", "bal": 0, "bankName": "SBI", "date": "2024-05-10T00:00:00"}, 
    {"accNo": "1282", "bal": 0, "bankName": "SBI", "date": "2024-05-10T00:00:00"}, 
    {"accNo": "1282", "bal": 0, "bankName": "SBI", "date": "2024-05-11T00:00:00"}, 
    {"accNo": "1282", "bal": 0, "bankName": "SBI", "date": "2024-05-12T00:00:00"}, 
    {"accNo": "1282", "bal": 0, "bankName": "SBI", "date": "2024-05-13T00:00:00"}, 
    {"accNo": "1282", "bal": 0, "bankName": "SBI", "date": "2024-05-14T00:00:00"}, 
    {"accNo": "1282", "bal": 0, "bankName": "SBI", "date": "2024-05-14T00:00:00"}, 
    {"accNo": "1282", "bal": 0, "bankName": "SBI", "date": "2024-05-14T00:00:00"}
]

const accAndBal = data.map((item) => {
    let bankName = item?.address?.split('-')[1];
    bankName = getBankName(bankName);

    const accNo = getAccNoAndBal(item.body, bankName);

    //console.log(accNo + " " + bal);

    /*const filterItems = accNo && userBalanceItems?.find((key) => key.accNo === accNo || key.accNo === accNo[0]?.slice(-4) && key.bankName === bankName);
    if (filterItems) {
        return { accNo: accNo[0].length > 4 ? accNo[0]?.slice(-4) : accNo, bal: parseFloat(filterItems?.bal), date: filterItems?.date, bankName: bankName }
    }
    else if (accNo && item?.date) {
        return { accNo: accNo[0].length > 4 ? accNo[0]?.slice(-4) : accNo, bal: parseFloat(bal) > 0 ? bal : 0, date: item?.date, bankName: bankName }
    }*/
    })

//console.log(accAndBal);

