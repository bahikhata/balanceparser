
const regx1 = /trf to\s+([^\s]+)\s+/;
const regx2 = /Transferred to ([^.]+)\./;
const regx3 = /Transferred to ((?:Mr\.|Ms\.|Mrs\.|Miss|Dr\.|Prof\.|Rev\.|Hon\.|Eng\.|Sr\.|Jr\.|PhD|MD|LLD|DSc|DDS|OD)\s*\S+)\./;
const regx4 = /Deposit by transfer from (.+?)\./;
const regx5 = /-([^-.]+)\s*$/
const regx6 = /through NEFT with UTR [^\s-]+ by (\S+)\s/;
const regx7 = /sent to ([^\s-]+)\s/;
const regx8 = /credited to ([^\s-]+)\s/
const regx9 = /trf to ([^\s.]+)\s/;

const getPartyName =  (message, bank) => {

    if (bank === "SBI") {
        const match1 = message.match(regx1);
        const match2 = message.match(regx2);
        const match3 = message.match(regx3);
        const match4 = message.match(regx4);
        const match5 = message.match(regx5);
        const match6 = message.match(regx6);
        const match7 = message.match(regx7);
        const match8 = message.match(regx8);
        const match9 = message.match(regx9);

        if (match1) {
            return match1[1];
        }
        if (match2) {
            return match2[1];
        }
        if (match3) {
            return match3[1];
        }
        if (match4) {
            return match4[1];
        }
        if (match5) {
            return match5[1];
        }
        if (match6) {
            return match6[1];
        }
        if (match7) {
            return match7[1];
        }
        if (match8) {
            return match8[1];
        }
        if (match9) {
            return match9[1];
        }

        return '.....'
    }

    return '.....'
}

module.exports = getPartyName;