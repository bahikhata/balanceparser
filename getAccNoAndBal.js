let regxBal1 = /Avl\s+Bal\s+INR\s+([\d,]+\.\d{2})/;
let regxBal2 = /Avl\s+Bal\s+Rs\s+([\d,]+\.\d{2})/;
let regxBal3 = /Avl\s+Balance\s+INR\s+([\d,]+\.\d{2})/
let regxBal4 = /Avlbl\sAmt:Rs\.(\d+\.\d+)/;

const getTransactionInfo = require('transaction-sms-parser');

const getAccNoAndBal = (message, bankName) => {

    if (bankName === 'SBI' || bankName==='BOB') {
        let bal1 = message.match(regxBal1);
        let bal2 = message.match(regxBal2);
        let bal3 = message.match(regxBal3);
      
        let bal4 = regxBal4.exec(message);

        if (bal4) {
            bal4 = bal4[1];
        }
        if (bal2) {
            bal2 = bal2[1];
        }
        if (bal3) {
            bal3 = bal3[1];
        }
        
        const bal = bal1 || bal2 || bal3 || bal4;
        return bal;

    }

    else {
        try {
            return getTransactionInfo(message)?.balance?.available;
        } catch (error) {
            return null 
        }
    }

}

module.exports = getAccNoAndBal;