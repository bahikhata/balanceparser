var getTransactionInfo = require('transaction-sms-parser');
const getAmount = require("./getAmount");
const getRefanceNo = require("./getRefanceNo");
const getPartyName = require("./getPartyName");

const extractData = (body, bankName) => {
    if (bankName==='SBI'){
        const amount = getAmount(body, bankName);
        const type =  body.includes('credited') ? 'credited' : body.includes('Credited')? "credited":'debited' ;
        const refNo = getRefanceNo(body, bankName);
        const Mode = body.includes('IMPS') ? 'IMPS' : body.includes('NEFT') ? 'NEFT' : 'UPI';
        const partyName = getPartyName(body, bankName);
        
        return { amount, type, refNo, Mode, partyName };
    }
    else {
        try {
            const data = getTransactionInfo(message);
            if (data) {

                const account = data.account.number;
                const amount = data.transaction.amount;
                const date = new Date().toDateString();
                const partyName = data.transaction.merchant || "";
                const refNo = data.transaction.referenceNo || "";
                const type = data.transaction.type === "credit" ? "credited" : "debited";

                return { account, amount, date, partyName, refNo, type };

            }
            else {
                return {}
            }
        } catch (error) {
            console.log(error)
        }

    }
}

module.exports = extractData;