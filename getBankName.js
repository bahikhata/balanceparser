const getBankName = (address) => {
    if (address?.includes('SBI')) {
        return 'SBI';
    }
    if(address?.includes('BOB')){
        return 'BOB'
    }
    if (address?.includes('ICICI') || address?.includes('ICI')) {
        return 'ICICI';
    }
    if (address?.includes('HDFC')) {
        return 'HDFC';
    }
    if (address?.includes('AXIS')) {
        return 'AXIS';
    }
    if (address?.includes('IDFC')) {
        return 'IDFC';
    } else {
        return 'Other';
    }
}

module.exports = getBankName;