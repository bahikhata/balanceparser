const regx1 = /debited by (\d+(\.\d+)?)/;
const regx2 = /credited by (\d+(\.\d+)?)/;
const regx3 = /credited by Rs(\d+(\.\d+)?)/;
const regx4 = /debited with INR ([\d,]+\.\d{2})/;
const regx5 = /NEFT of Rs ([\d,]+\.\d{2})/;
const regx6 = /Credited INR ([\d,]+\.\d{2})/
const regx7 = /Rs\.(\d+(?:,\d+)*\.\d+)/;
const regx8 = /INR\s+(\d+(?:,\d+)*\.\d+)/
const regx9 = /debitedby (\d+(\.\d+)?)/
const regx10 = /debit by NACH of Rs (\d{1,3}(,\d{3})*(\.\d{2})?)/
const regx11 = /Rs\. (\d+(\.\d{2})?)/;

const getAmount = (message, bankName) => {
    
    if (bankName === "SBI") {
        const match1 = message.match(regx1);
        const match2 = message.match(regx2);
        const match3 = message.match(regx3);
        const match4 = message.match(regx4);
        const match5 = message.match(regx5);
        const match6 = message.match(regx6);
        const match7 = message.match(regx7);
        const match8 = message.match(regx8);
        const match9 = message.match(regx9);
        const match10 = message.match(regx10);
        const match11 = message.match(regx11);

        if (match1) {
            return parseFloat(match1[1].replace(/,/g, ""));
        }
        if (match2) {
            return parseFloat(match2[1].replace(/,/g, ""));
        }
        if (match3) {
            return parseFloat(match3[1].replace(/,/g, ""));
        }
        if (match4) {
            return parseFloat(match4[1].replace(/,/g, ""));
        }
        if (match5) {
            return parseFloat(match5[1].replace(/,/g, ""));
        }
        if (match6) {
            return parseFloat(match6[1].replace(/,/g, ""));
        }
        if (match7) {
            return parseFloat(match7[1].replace(/,/g, ""));
        }
        if (match8) {
            return parseFloat(match8[1].replace(/,/g, ""));
        }
        if (match9) {
            return parseFloat(match9[1].replace(/,/g, ""));
        }
        if (match10) {
            return parseFloat(match10[1].replace(/,/g, ""));
        }
        if (match11) {
            return parseFloat(match11[1].replace(/,/g, ""));
        }

        return 0;
    }
    else {
        return 0;
    }
}

module.exports = getAmount;